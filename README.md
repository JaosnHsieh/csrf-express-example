## CSRF protection in Express.js

###references

https://github.com/expressjs/csurf

https://github.com/pillarjs/understanding-csrf

http://huli.logdown.com/posts/1550395-csrf-introduction

https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)


### Getting start

##### demo site

`npm i`

`cd ./1.bareSite`

`node app.js`

and 2, 3, 4, 5

default account info 
username: mi
password: 123


##### attacker site

`cd attackerSite`

`npm i -g live-server`

`live-server`
 or whatever simple http static server