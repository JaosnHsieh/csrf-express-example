// when you add cors, content-type checker no logner protect website from csrf attack
// you can do whitelist check by http origin or referer or csrf token check
var csrf = require("csurf");
var csrfProtection = csrf({ cookie: false });
var cors = require("cors");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var expressSession = require("express-session");
var express = require("express");
var app = express();
var nunjucks = require("nunjucks");
var fs = require("fs");
var list;
function readList() {
  list = fs.readFileSync("../list.json");
  list = JSON.parse(list.toString());
  return list;
}
function saveToList(item) {
  list.push({
    _id: item._id,
    name: item.name
  });
  fs.writeFileSync("../list.json", JSON.stringify(list));
}

// set up middlewares
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressSession({ secret: "keyboard cat", cookie: { maxAge: 60000 } }));
nunjucks.configure("views", {
  autoescape: true,
  express: app,
  watch: true
});

// routes

app.get("/", function(req, res) {
  if (req.session.logined) {
    return res.redirect("/list");
  }
  res.redirect("login");
});

app.get("/login", function(req, res) {
  if (req.session.logined) {
    return res.redirect("/list");
  }
  res.render("login.html");
});

app.post("/login", function(req, res) {
  if (req.body.username === "mi" && req.body.password === "123") {
    req.session.logined = true;
    return res.redirect("/list");
  }
  return res.status(400).send("wrong user or password");
});

app.get("/list", csrfProtection, function(req, res) {
  if (!req.session.logined) {
    return res.redirect("/login");
  }
  list = readList();
  res.render("list.html", { list: list, csrfToken: req.csrfToken() });
});

// protected api endpoint by check content-type

app.post(
  "/list",
  csrfProtection,
  function originChecker(req, res, next) {
    var whiteListSite = ["http://localhost:3000"];
    var origin = req.headers.origin || req.headers.referer;
    if (!whiteListSite.includes(origin)) {
      return res.status(400).send("origin or referer whiltelist failed!!");
    }
    return next();
  },
  function contentTypeChecker(req, res, next) {
    //https://stackoverflow.com/questions/23271250/how-do-i-check-content-type-using-expressjs
    var contype = req.headers["content-type"];
    if (!contype || contype.indexOf("application/json") !== 0) {
      return res.status(400).send("please use application/json content type");
    }
    return next();
  },
  function(req, res) {
    console.log("req.body", req.body);
    saveToList(req.body);
    res.send(200);
  }
);

app.get("/logout", function(req, res) {
  req.session.logined = false;
  res.send("logouted! <a href='../login'> login </a>");
});

var port = 3000;
app.listen(port, () => {
  console.log("Express listening to " + port);
});
