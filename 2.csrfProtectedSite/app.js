// csrf step 1
var csrf = require("csurf");
var csrfProtection = csrf({ cookie: false });

var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var expressSession = require("express-session");
var express = require("express");
var app = express();
var nunjucks = require("nunjucks");
var bodyParser = bodyParser.urlencoded({ extended: false });
var fs = require("fs");
var list;
function readList() {
  list = fs.readFileSync("../list.json");
  list = JSON.parse(list.toString());
  return list;
}
function saveToList(item) {
  list.push({
    _id: item._id,
    name: item.name
  });
  fs.writeFileSync("../list.json", JSON.stringify(list));
}

// set up middlewares
app.use(bodyParser);
app.use(cookieParser());
app.use(expressSession({ secret: "keyboard cat", cookie: { maxAge: 60000 } }));
nunjucks.configure("views", {
  autoescape: true,
  express: app,
  watch: true
});

// routes

app.get("/", function(req, res) {
  if (req.session.logined) {
    return res.redirect("/list");
  }
  res.redirect("login");
});

app.get("/login", function(req, res) {
  if (req.session.logined) {
    return res.redirect("/list");
  }
  res.render("login.html");
});

app.post("/login", function(req, res) {
  if (req.body.username === "mi" && req.body.password === "123") {
    req.session.logined = true;
    return res.redirect("/list");
  }
  return res.status(400).send("wrong user or password");
});

// csrf step 2
app.get("/list", csrfProtection, function(req, res) {
  if (!req.session.logined) {
    return res.redirect("/login");
  }
  list = readList();
  res.render("list.html", { list: list, csrfToken: req.csrfToken() });
});

// csrf step 3
app.post("/list", csrfProtection, function(req, res) {
  saveToList(req.body);
  res.send(200);
});

app.get("/logout", function(req, res) {
  req.session.logined = false;
  res.send("logouted! <a href='../login'> login </a>");
});

var port = 3000;
app.listen(port, () => {
  console.log("Express listening to " + port);
});
