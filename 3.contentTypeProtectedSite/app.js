// protected csrf attack without csrf token by setting up content-type check
// https://github.com/pillarjs/understanding-csrf#conclusion
// http://huli.logdown.com/posts/1550395-csrf-introduction 如果你在你的後端 Server 有檢查這個 content type 的話，是可以避免掉上面這個攻擊的。

var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var expressSession = require("express-session");
var express = require("express");
var app = express();
var nunjucks = require("nunjucks");
var fs = require("fs");
var list;
function readList() {
  list = fs.readFileSync("../list.json");
  list = JSON.parse(list.toString());
  return list;
}
function saveToList(item) {
  list.push({
    _id: item._id,
    name: item.name
  });
  fs.writeFileSync("../list.json", JSON.stringify(list));
}

// set up middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressSession({ secret: "keyboard cat", cookie: { maxAge: 60000 } }));
nunjucks.configure("views", {
  autoescape: true,
  express: app,
  watch: true
});

// routes

app.get("/", function(req, res) {
  if (req.session.logined) {
    return res.redirect("/list");
  }
  res.redirect("login");
});

app.get("/login", function(req, res) {
  if (req.session.logined) {
    return res.redirect("/list");
  }
  res.render("login.html");
});

app.post("/login", function(req, res) {
  if (req.body.username === "mi" && req.body.password === "123") {
    req.session.logined = true;
    return res.redirect("/list");
  }
  return res.status(400).send("wrong user or password");
});

app.get("/list", function(req, res) {
  if (!req.session.logined) {
    return res.redirect("/login");
  }
  list = readList();
  res.render("list.html", { list: list });
});

// protected api endpoint by check content-type

app.post(
  "/list",
  function contentTypeChecker(req, res, next) {
    //https://stackoverflow.com/questions/23271250/how-do-i-check-content-type-using-expressjs
    var contype = req.headers["content-type"];
    if (!contype || contype.indexOf("application/json") !== 0) {
      return res.status(400).send("please use application/json content type");
    }
    return next();
  },
  function(req, res) {
    console.log("req.body", req.body);
    saveToList(req.body);
    res.send(200);
  }
);

app.get("/logout", function(req, res) {
  req.session.logined = false;
  res.send("logouted! <a href='../login'> login </a>");
});

var port = 3000;
app.listen(port, () => {
  console.log("Express listening to " + port);
});
